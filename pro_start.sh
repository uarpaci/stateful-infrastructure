#!/bin/bash


# Recieve hostname as the first argument, Use it "as is"
HNAME=$1
echo ${HNAME} > /etc/hostname

#Update Hostname
/bin/sh /etc/init.d/hostname.sh

sed -i s/"localhost"/"localhost ${HOSTNAME} ${HOSTNAME}.domain.net"/g /etc/hosts


echo "deb http://debian.saltstack.com/debian wheezy-saltstack main" >> /etc/apt/sources.list.d/saltstack.list
sudo apt-key add /tmp/debian-salt-team-joehealy.gpg.key
sudo apt-get update && sudo apt-get install salt-minion -y
echo ${HNAME} > /etc/salt/minion_id
sed -i s/"#master: salt"/"master: salt-master.domain.net"/g /etc/salt/minion
service salt-minion restart
