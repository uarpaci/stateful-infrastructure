variable "ec2name"  {
  }
resource "aws_instance" "micro" {
  ami                     = "ami-XXX"
  instance_type           = "t2.small"
  availability_zone       = "eu-central-1b"
  key_name                = "saltstack"
  vpc_security_group_ids  = [ "sg-XXX" ]
  subnet_id               = "subnet-XXX"
  source_dest_check       = true
  associate_public_ip_address = true

  tags                    = {
      Name   = "${var.ec2name}"
  }

  provisioner "file" {
    source = "debian-salt-team-joehealy.gpg.key"
    destination = "/tmp/debian-salt-team-joehealy.gpg.key"
    connection {
        user      = "admin"
        port      = "22"
        key_file  = "saltstack.pem"
        host      = "${aws_instance.micro.private_ip}"
      
    }
  }

  provisioner "file" {
    source = "pro_start.sh"
    destination = "/tmp/pro_start.sh"
    connection {
        user      = "admin"
        port      = "22"
        key_file  = "saltstack.pem"
        host      = "${aws_instance.micro.private_ip}"
      
    }
  }

  provisioner "remote-exec" {
    inline = ["sudo /bin/sh /tmp/pro_start.sh ${var.ec2name}"]
    connection {
        user      = "admin"
        port      = "22"
        key_file  = "saltstack.pem"
        host      = "${aws_instance.micro.private_ip}"
      
    }
  }

}

resource "aws_eip" "ip" {
    instance = "${aws_instance.micro.id}"
}
