/*

  The following data are static accross AWS account
  access_key
  secret_key
  region

  Therefore state them separately

*/

provider "aws" {
    access_key = "XXX"
    secret_key = "XXX"
    region     = "eu-central-1"
}
